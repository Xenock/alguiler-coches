json.array!(@rents) do |rent|
  json.extract! rent, :id, :start, :end
  json.url rent_url(rent, format: :json)
end
