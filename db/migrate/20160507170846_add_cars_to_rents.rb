class AddCarsToRents < ActiveRecord::Migration
  def change
    add_reference :rents, :car, index: true, foreign_key: true
  end
end
