class AddColumnsToCar < ActiveRecord::Migration
  def change
    add_column :cars, :passengers, :integer
    add_column :cars, :trunk, :string
    add_column :cars, :doors, :string
    add_column :cars, :plate, :string
    add_column :cars, :transmission, :string
  end
end
